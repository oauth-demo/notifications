json.extract! notification, :id, :text, :shown, :created_at, :updated_at
json.url notification_url(notification, format: :json)
